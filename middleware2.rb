require 'awesome_print'

class Middleware2

	include Rack::Utils


	def initialize(app)
		@app = app
	end


	def call(env)
		start = Time.now

		@status, @headers, @response = @app.call(env)

		stop = Time.now

		if @headers['Content-Type'].include?('text/html')
			@response = ["Reporting from middleware 2: #{stop - start}<br>#{@response[0]}"]
			@headers['Content-Length'] = @response[0].length.to_s
		end

		[@status, @headers, @response]
	end


end