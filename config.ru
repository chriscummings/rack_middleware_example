require './app'
require './middleware'
require './middleware2'

# Middleware pipeline
use Rack::Reloader
use Middleware1
use Middleware2

# Actual application
run App.new