require 'awesome_print'

class Middleware1

	include Rack::Utils


	def initialize(app)
		@app = app
	end


	def call(env)
		#env includes all request and environment data.
		ap env

		# Convenience class to handle request.
		# http://www.rubydoc.info/gems/rack/Rack/Request
		req = Rack::Request.new(env)
		ap req.methods

		start = Time.now

		# Get response from child/next in line middleware or app.
		@status, @headers, @response = @app.call(env)

		stop = Time.now

		# Format response.
		if @headers['Content-Type'].include?('text/html')
			@response = ["Reporting from middleware 1: #{stop - start}<br>#{@response[0]}"]
			@headers['Content-Length'] = @response[0].length.to_s
		end

		# Return response to "parent" middleware or user.
		[@status, @headers, @response]
	end


end
